from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render

def index(request):
    return render(request, "index.html")


def profile(request):
    return render(request, "profile.html")


def project(request):
    return render(request, "project.html")


def story1(request):
    return render(request, "story1.html")
