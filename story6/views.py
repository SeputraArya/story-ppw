from django.shortcuts import render, redirect, get_object_or_404
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm

# Create your views here.


def landing(request):
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/kegiatan/')
    else:
        form = KegiatanForm()
    return render(request, 'landing.html', {'form': form})


def listAcara(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()

    return render(request, 'hasilKegiatan.html', {'kegiatan': kegiatan, 'peserta': peserta})


def register(request, index):
    if request.method == 'POST':
        form = PesertaForm(request.POST)
        if form.is_valid():
            pesertaBaru = Peserta(Ikut_Kegiatan=Kegiatan.objects.get(
                id=index), Nama_Peserta=form.data['Nama_Peserta'])
            pesertaBaru.save()
            return redirect('../../')
    else:
        form = PesertaForm()
    return render(request, 'tambahPeserta.html', {'form': form})


def hapus_nama(request, index):
    obj_temp = get_object_or_404(Peserta, id=index)  # mengambilnamaobject

    if request.method == 'POST':
        obj_temp.delete()
        return redirect('../')

    context = {
        'object_hapus': obj_temp
    }

    return render(request, "hapusNama.html", context)
