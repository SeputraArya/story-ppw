from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    Nama_Kegiatan = models.CharField(max_length=120)


class Peserta(models.Model):
    Nama_Peserta = models.CharField(max_length=120)
    Ikut_Kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE,  null=True, blank=True)