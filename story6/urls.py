from django.urls import path, include
from django.shortcuts import render
from django.conf.urls import url
from .views import *

app_name = 'story6'
urlpatterns = [
    path('kegiatan/', landing, name='landing'),
    path('kegiatan/hasil/', listAcara, name='listAcara'),
    path('kegiatan/hasil/register/<int:index>/', register, name='register'),
    path('kegiatan/hasil/<int:index>/', hapus_nama, name='hapus_nama'),

]
