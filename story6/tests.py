from django.test import TestCase, Client
from django.urls import resolve
from .views import listAcara, landing, register
from .models import Kegiatan, Peserta


# Create your tests here.
class TestKegiatan(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/kegiatan/hasil/')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/kegiatan/hasil/')
        self.assertEqual(found.func, listAcara)

    def test_event_using_template(self):
        response = Client().get('/kegiatan/hasil/')
        self.assertTemplateUsed(response, 'hasilKegiatan.html')


class TestTambahKegiatan(TestCase):
    def test_add_event_url_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_add_event_index_func(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, landing)

    def test_add_event_using_template(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_event_model_create_new_object(self):
        acara = Kegiatan(Nama_Kegiatan="abc")
        acara.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)

    def test_event_url_post_is_exist(self):
        response = Client().post(
            '/kegiatan/', data={'Nama_Kegiatan': 'berenang'})
        self.assertEqual(response.status_code, 302)


class TestRegist(TestCase):
    def setUp(self):
        acara = Kegiatan(Nama_Kegiatan="abc")
        acara.save()

    def test_regist_url_post_is_exist(self):
        response = Client().post('/kegiatan/hasil/register/1/',
                                data={'Nama_Peserta': 'arya'})
        self.assertEqual(response.status_code, 302)

    def test_regist_url_is_exist(self):
        response = Client().get('/kegiatan/hasil/register/1/')
        self.assertEqual(response.status_code, 200)


class TestHapusNama(TestCase):
    def setUp(self):
        acara = Kegiatan(Nama_Kegiatan="abc")
        acara.save()
        nama = Peserta(Nama_Peserta="cba")
        nama.save()

    def test_hapus_url_post_is_exist(self):
        response = Client().post('/kegiatan/hasil/1/')
        self.assertEqual(response.status_code, 302)

    def test_hapus_url_is_exist(self):
        response = Client().get('/kegiatan/hasil/1/')
        self.assertEqual(response.status_code, 200)
