$(document).ready(function () {
    $("#search-input").on("keyup", function (e) {
        var q = e.currentTarget.value.toLowerCase()
        console.log(q); //q is the search query
        $.ajax({
            url: "book?q=" + q,
            success: function (response) {
                $('#search-result').html('')
                var result = '';
                for (var i = 0; i < response.items.length; i++) {
                    item = response.items[i].volumeInfo;

                    result += "<tr> <th scope='row' class='align-center text-center'>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" +
                        item.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td class='align-center'>" + item.title + "</td>" +
                        "<td class='align-center'>" + item.authors + "</td>" +
                        "<td class='align-center'>" + item.publisher + "</td>" +
                        "<td class='align-center'>" + item.publishedDate + "</td>" +
                        "<td class='align-center'>" +
                        "</td>" + "</tr>"
                }
                $('#search-result').append(result);
            }
        });
    });
});
