from django.conf.urls import url
from django.urls import path
from .views import *
from . import views
app_name = 'story8'

urlpatterns = [
    path('story8/', views.story8),
    path('story8/book/', views.bookAPI),
]
