from django import forms
from . import models


class formMatkul(forms.Form):
    mataKuliah = forms.CharField(max_length=300, required=True)
    dosenPengajar = forms.CharField(max_length=300, required=True)
    jumlahSKS = forms.CharField(max_length=300, required=True)
    deskripsi = forms.CharField(max_length=300, required=True)
    semester = forms.CharField(max_length=300, required=True)
