from django.urls import path,include
from django.shortcuts import render
from django.conf.urls import url
from . import views

app_name='story5'
urlpatterns=[
	path('jadwal/',views.createJadwal,name='jadwal'),
	url(r'^delete/(?P<table_id>[0-9]+)/$',views.deleteJadwal, name='deleteJadwal'),

]