from django.db import models

class modelMatkul(models.Model):
	mataKuliah = models.TextField(max_length=300)
	dosenPengajar = models.TextField(max_length=300)
	jumlahSKS = models.TextField(max_length=300)
	deskripsi = models.TextField(max_length=300)
	semester = models.TextField(max_length=300)