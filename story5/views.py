from django.shortcuts import render, redirect
from .forms import formMatkul
from .models import modelMatkul


def landing(request):
    return render(request, 'landing.html')


def createJadwal(request):
    matkul = modelMatkul.objects.all()
    createMatkul = formMatkul(request.POST or None)  # POST vs GET
    context = {
        'matkul': matkul,
        'createMatkul': createMatkul
    }

    if request.method == 'POST':
        if createMatkul.is_valid():
            matkul.create(
                mataKuliah=createMatkul.cleaned_data.get('mataKuliah'),
                dosenPengajar=createMatkul.cleaned_data.get('dosenPengajar'),
                jumlahSKS=createMatkul.cleaned_data.get('jumlahSKS'),
                deskripsi=createMatkul.cleaned_data.get('deskripsi'),
                semester=createMatkul.cleaned_data.get('semester'),
            )
    return render(request, 'jadwal.html', context)


def deleteJadwal(request, table_id=None):
    matkul = modelMatkul.objects.get(id=table_id)
    matkul.delete()
    return redirect('/jadwal/')
# Create your views here.
